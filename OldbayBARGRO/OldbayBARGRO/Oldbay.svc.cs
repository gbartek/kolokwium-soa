﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Models;

namespace OldbayBARGRO
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class OldbayService : IOldbayService
    {
        private readonly Repository.Repository repo = new Repository.Repository();
        private readonly Logger logger = new Logger();

        public string CreateWing(Wing wing)
        {
            logger.Write($"WCF Creating wing '{wing.Name}'", LogLevel.INFO);
            return this.repo.Add(wing);
        }

        public Wing GetWing(string name)
        {
            logger.Write($"WCF Get wing ${name}", LogLevel.INFO);
            return this.repo.GetWing(name);
        }

        public List<Wing> GetAllWings()
        {
            logger.Write($"WCF Get all wings", LogLevel.INFO);
            return this.repo.GetAll();
        }
    }
}
