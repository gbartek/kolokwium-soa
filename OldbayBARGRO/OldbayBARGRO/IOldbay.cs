﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace OldbayBARGRO
{
    [ServiceContract]
    public interface IOldbayService
    {
        [OperationContract]
        Wing GetWing(string name);

        [OperationContract]
        String CreateWing(Wing wing);


        [OperationContract]
        List<Wing> GetAllWings();
    }
}
