﻿using LiteDB;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class Repository
    {
        private readonly string databaseName = "wings.db";
        private readonly string collectionName = "wings";
        public string Add(Wing wing)
        {
            using (var db = new LiteDatabase(databaseName))
            {
                var repository = db.GetCollection<Wing>(collectionName);
                var existing = repository.FindOne(w => w.Name == wing.Name);
                if (existing == null)
                {
                    repository.Insert(wing);
                }
                return wing.Name;
            }
        }

        public Wing GetWing(string WingName)
        {
            using (var db = new LiteDatabase(databaseName))
            {
                var repository = db.GetCollection<Wing>(collectionName);
                return repository.FindOne(w => w.Name == WingName);
            }
        }

        public List<Wing> GetAll()
        {
            using (var db = new LiteDatabase(databaseName))
            {
                var repository = db.GetCollection<Wing>(collectionName);
                return repository.FindAll().ToList(); ;
            }
        }
    }
}
