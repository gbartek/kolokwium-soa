﻿using System;

namespace Models
{
    public class Wing
    {
        public int Id { get; set; }
        public int Power { get; set; }
        public int Shield { get; set; }
        public string Name { get; set; }
    }
}
