﻿using Newbay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newbay.Services
{
    public interface IWingsService
    {
        int Add(Wing wing);

        bool Delete(int id);

        Wing Get(int id);

        List<Wing> GetAll();

        bool Update(Wing wing);
    }
}
