﻿using LiteDB;
using Newbay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newbay.Services
{
    public class WingsService : IWingsService
    {
        private readonly string database = @"wings.db";
        private readonly string collection = "wings";
        public int Add(Wing wing)
        {
            {
                using (var db = new LiteDatabase(this.database))
                {
                    var repository = db.GetCollection<Wing>(this.collection);
                    if (repository.FindById(wing.Id) != null)
                        repository.Update(wing);
                    else
                        repository.Insert(wing);

                    return wing.Id;
                }
            }
        }

        public bool Delete(int id)
        {
            {
                using (var db = new LiteDatabase(this.database))
                {
                    var repository = db.GetCollection<Wing>(this.collection);
                    return repository.Delete(id);
                }
            }
        }

        public Wing Get(int id)
        {
            using (var db = new LiteDatabase(this.database))
            {
                var repository = db.GetCollection<Wing>(this.collection);
                return repository.FindById(id);
            }
        }

        public List<Wing> GetAll()
        {
            using (var db = new LiteDatabase(this.database))
            {
                var repository = db.GetCollection<Wing>(this.collection);
                return repository.FindAll().ToList();
            }
        }

        public bool Update(Wing wing)
        {
            using (var db = new LiteDatabase(this.database))
            {
                var repository = db.GetCollection<Wing>(this.collection);
                return repository.Update(wing);
            }
        }
    }
}