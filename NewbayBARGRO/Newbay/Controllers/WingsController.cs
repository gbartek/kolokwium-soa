﻿using Newbay.Models;
using Newbay.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Newbay.Controllers
{
    public class WingsController : ApiController
    {
        private readonly IWingsService WingsService;
        private readonly ILogger Logger;

        public WingsController(IWingsService service, ILogger logger)
        {
            this.WingsService = service;
            this.Logger = logger;
        }
        // GET api/<controller>
        public List<Wing> Get()
        {
            Logger.Write($"[GET] all", LogLevel.INFO);
            return WingsService.GetAll();
        }

        // GET api/<controller>/5
        [ResponseType(typeof(Wing))]
        public IHttpActionResult Get(int id)
        {
            Logger.Write($"[GET] /{id}", LogLevel.INFO);
            var wing = WingsService.Get(id);
            if (wing == null)
            {
                return NotFound();
            }
            return Ok(wing);
        }

        // POST api/<controller>
        [ResponseType(typeof(int))]
        public IHttpActionResult Post([FromBody]Wing wing)
        {
            Logger.Write($"[POST]", LogLevel.INFO);
            WingsService.Add(wing);
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var id = WingsService.Add(wing);
            return Ok(id);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]Wing value)
        {
            Logger.Write($"[PUT] /{id}", LogLevel.INFO);
            WingsService.Update(value);
        }

        public void Put([FromUri]int id, int power, int shield, string name)
        {
            Logger.Write($"[PUT] uri", LogLevel.INFO);
            WingsService.Update(new Wing() { Id = id, Power = power, Shield = shield, Name = name });
        }

        // DELETE api/<controller>/5
        [ResponseType(typeof(Wing))]
        public IHttpActionResult Delete(int id)
        {
            Logger.Write($"[DELETE] /{id}", LogLevel.INFO);

            var wing = WingsService.Get(id);
            if (wing == null)
                return NotFound();

            WingsService.Delete(id);
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}