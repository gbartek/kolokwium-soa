﻿using OldbayBARGRO;
using StardestroyerBeta.OldbayService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StardestroyerBeta
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = new Logger();

            logger.Write("Creating service", LogLevel.INFO);
            OldbayServiceClient client = new OldbayServiceClient();

            logger.Write("Creating Wing", LogLevel.INFO);
            client.CreateWing(new Wing() { Name = "Wingoose", Power = 12, Shield = 100 });

            logger.Write("Fetching wing", LogLevel.INFO);
            Wing wing = client.GetWing("Wingoose");
            Console.WriteLine(wing.Name);
        }
    }
}
