﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StardestroyerBeta.OldbayService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Wing", Namespace="http://schemas.datacontract.org/2004/07/Models")]
    [System.SerializableAttribute()]
    public partial class Wing : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int PowerField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ShieldField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Power {
            get {
                return this.PowerField;
            }
            set {
                if ((this.PowerField.Equals(value) != true)) {
                    this.PowerField = value;
                    this.RaisePropertyChanged("Power");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Shield {
            get {
                return this.ShieldField;
            }
            set {
                if ((this.ShieldField.Equals(value) != true)) {
                    this.ShieldField = value;
                    this.RaisePropertyChanged("Shield");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="OldbayService.IOldbayService")]
    public interface IOldbayService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOldbayService/GetWing", ReplyAction="http://tempuri.org/IOldbayService/GetWingResponse")]
        StardestroyerBeta.OldbayService.Wing GetWing(string name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOldbayService/GetWing", ReplyAction="http://tempuri.org/IOldbayService/GetWingResponse")]
        System.Threading.Tasks.Task<StardestroyerBeta.OldbayService.Wing> GetWingAsync(string name);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOldbayService/CreateWing", ReplyAction="http://tempuri.org/IOldbayService/CreateWingResponse")]
        string CreateWing(StardestroyerBeta.OldbayService.Wing wing);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IOldbayService/CreateWing", ReplyAction="http://tempuri.org/IOldbayService/CreateWingResponse")]
        System.Threading.Tasks.Task<string> CreateWingAsync(StardestroyerBeta.OldbayService.Wing wing);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IOldbayServiceChannel : StardestroyerBeta.OldbayService.IOldbayService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class OldbayServiceClient : System.ServiceModel.ClientBase<StardestroyerBeta.OldbayService.IOldbayService>, StardestroyerBeta.OldbayService.IOldbayService {
        
        public OldbayServiceClient() {
        }
        
        public OldbayServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public OldbayServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public OldbayServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public OldbayServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public StardestroyerBeta.OldbayService.Wing GetWing(string name) {
            return base.Channel.GetWing(name);
        }
        
        public System.Threading.Tasks.Task<StardestroyerBeta.OldbayService.Wing> GetWingAsync(string name) {
            return base.Channel.GetWingAsync(name);
        }
        
        public string CreateWing(StardestroyerBeta.OldbayService.Wing wing) {
            return base.Channel.CreateWing(wing);
        }
        
        public System.Threading.Tasks.Task<string> CreateWingAsync(StardestroyerBeta.OldbayService.Wing wing) {
            return base.Channel.CreateWingAsync(wing);
        }
    }
}
